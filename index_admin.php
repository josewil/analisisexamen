<?php
 	session_start();
	if($_SESSION["logueado"] == TRUE) {
        $id_encargado=$_SESSION["id"];
        ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="boostrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">


    <title>Document</title>

    <style>
body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}



.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
</head>
<body>
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#"><img src="img/logof.png" alt="logo" width="250"></a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="nosotros.html">Nosotros</a>
                </li>
                <li class="nav-item">
                  
				</li>
				<li class="nav-item">
                        <a class="nav-link font-weight-bold" href="#"  onclick="document.getElementById('id01').style.display='block'">Agregar producto</a>
               
				</li>
				<li class="nav-item">
                    <a class="nav-link font-weight-bold" href="salir.php">Salir</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <a class="nav-link font-weight-bold" href="#">
                    <?php
                require("conexion.php");
                $consulta = "SELECT * FROM administrador WHERE id_admin=$id_encargado";
                if($resultado = $enlace->query($consulta)) {
                  while($row = $resultado->fetch_array()) {
                    
                    echo $row["nombre_admin"];
                    echo " ".$row["apellido_admin"]." ";
                  }
                  $resultado->close();
                }
        
              ?>

                    </a>
                </form>
            </div>
        </nav>
	</div>

	   
	<div class="wrap" id="contenedor">
		<h1>Escoge un producto</h1>
		<div class="store-wrapper">
			<div class="category_list">
				<a href="#" class="category_item" category="all">Todo</a>
				<a href="#" class="category_item" category="verduras">Verduras</a>
				<a href="#" class="category_item" category="Frutas">Frutas</a>
				<a href="#" class="category_item" category="Especias">Hierbas y Especias</a>
				<a href="#" class="category_item" category="Secos">Frutos Secos</a>
				<a href="#" class="category_item" category="listos">Listos para Cocinar</a>
			</div>
            <section class="products-list">
                    <?php
                                  require("conexion.php");
                                 
                                  $consulta = "SELECT * FROM producto";
                                  if($resultado = $enlace->query($consulta)) {
                                    $c=1;
                                    while($row = $resultado->fetch_array()) {?>
                                    <div class="product-item" category="<?php echo $row['categoria_producto'];?>">
                            <img src="<?php echo$row['imagen_producto'];?>" alt="" >
                            <a href="#"><?php echo $row["nombre_producto"];?></a>
                            
                        </div>
                                  <?php
                                     }
                                     $resultado->close();
                                   }
                                  ?>
                    
                        
                        
                    </section>
		</div>
	</div>

		<div id="publi">
				<img src="img/deli.gif" alt="300" ALIGN=RIGHT >
				<img src="img/carniceria.gif" alt="300" ALIGN=LEFT >
		</div>


   
    <script type="text/javascript" src="js/jquery-1.4.2.js" ></script>
	<script type="text/javascript" src="js/cufon-yui.js"></script>
	<script type="text/javascript" src="js/cufon-replace.js"></script>
	<script type="text/javascript" src="js/Avenir_900.font.js"></script>
	<script type="text/javascript" src="js/Avenir_300.font.js"></script>
    <script type="text/javascript" src="js/Avenir_500.font.js"></script>
    <script src="js/javascrip.js"></script>

   

<div id="id01" class="modal">
  
  <form class="modal-content animate" action="nuevo_producto.php" method="POST">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
      
    </div>

    <div class="container">
            <label for="uname"><b>Categoria</b></label>
           <select name="categoria" id="" class="form-control">
               <option value="verduras">Verduras</option>
               <option value="Frutas">Frutas</option>
               <option value="Especias">Hierbas y Especies</option>
               <option value="Secos">Frutos Secos</option>
               <option value="listos">Listos para Cocinar</option>
           </select>
      <label for="uname"><b>Nombre Producto</b></label>
      <input type="text" placeholder="" name="producto" required>

      <label for="psw"><b>Cantidad</b></label>
      <input type="text" placeholder="" name="cantidad" required>

      <label for="psw"><b>Precio</b></label>
      <input type="text" placeholder="" name="precio" required>

      <label for="psw"><b>Imagen</b></label>
      <input type="file" placeholder="" name="imagen" required>
        
      <button type="submit">Guardar</button>
      
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancelar</button>
      
    </div>
  </form>
</body>
</html>
<?php
} else {
    header("Location: login.html");
}
?>