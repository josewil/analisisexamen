
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="boostrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">


    <title>Document</title>
</head>
<body>
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#"><img src="img/logof.png" alt="logo" width="250"></a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="nosotros.html">Nosotros</a>
                </li>
                
				
			
                </ul>
                <form class="form-inline my-2 my-lg-0">
							
				<a href="login.html" class="btn btn-primary">Iniciar Sesion</a>

               
                </form>
            </div>
        </nav>
	</div>
	
	   <div class="row">
			<div class="col-md-12">
			<div class="wrap" id="contenedor">
		<h1>Escoge un producto</h1>
		<div class="store-wrapper">
			<div class="category_list">
				<a href="#" class="category_item" category="all">Todo</a>
				<a href="#" class="category_item" category="verduras">Verduras</a>
				<a href="#" class="category_item" category="Frutas">Frutas</a>
				<a href="#" class="category_item" category="Especias">Hierbas y Especias</a>
				<a href="#" class="category_item" category="Secos">Frutos Secos</a>
				<a href="#" class="category_item" category="listos">Listos para Cocinar</a>
			</div>
			<section class="products-list">
			<?php
                          require("conexion.php");
                         
                          $consulta = "SELECT * FROM producto";
                          if($resultado = $enlace->query($consulta)) {
                            $c=1;
                            while($row = $resultado->fetch_array()) {?>
                            <div class="product-item" category="<?php echo $row['categoria_producto'];?>" >
					<img src="<?php echo$row['imagen_producto'];?>" alt="" >
					<p class="text-center">500 Grm.
					<?php echo$row['precio_producto']."Bs.";?><br>
					<?php echo $row["nombre_producto"];?></p>
					<form action="compra.php" method="post">
						<input type="text" value="<?php echo $row['id_producto'];?>" name="id_producto" hidden>
						<input type="text" value="<?php echo $id_encargado?>" name="id_usuario" hidden> 
						<input type="text" value="<?php echo $row['precio_producto'];?>" name="precio" hidden>
					
					</form>
					
				</div>
                          <?php
                             }
                             $resultado->close();
                           }
                          ?>
			
				
				
			</section>
		</div>
	</div>
			</div>
			
			
		</div>
	
	
   
    <script type="text/javascript" src="js/jquery-1.4.2.js" ></script>
	<script type="text/javascript" src="js/cufon-yui.js"></script>
	<script type="text/javascript" src="js/cufon-replace.js"></script>
	<script type="text/javascript" src="js/Avenir_900.font.js"></script>
	<script type="text/javascript" src="js/Avenir_300.font.js"></script>
    <script type="text/javascript" src="js/Avenir_500.font.js"></script>
    <script src="js/javascrip.js"></script>
</body>
</html>
<?php
