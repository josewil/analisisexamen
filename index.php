<?php
 	session_start();
	if($_SESSION["logueado"] == TRUE) {
        $id_encargado=$_SESSION["id"];
        ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="boostrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">


    <title>Document</title>
</head>
<body>
    <div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <a class="navbar-brand" href="#"><img src="img/logof.png" alt="logo" width="250"></a>
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="nosotros.html">Nosotros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="noticias.php">
										<?php
                require("conexion.php");
                $consulta = "SELECT * FROM usuario WHERE id_usuario=$id_encargado";
                if($resultado = $enlace->query($consulta)) {
                  while($row = $resultado->fetch_array()) {
                    
                    echo $row["nombre_usuario"];
                    echo " ".$row["apellido_usuario"]." ";
                  }
                  $resultado->close();
                }
        
              ?>
										</a>
				</li>
				
				<li class="nav-item">
                    <a class="nav-link font-weight-bold" href="salir.php">Salir</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
									<a class="nav-link font-weight-bold" href="noticias.php">
										<?php
                require("conexion.php");
                $consulta = "SELECT * FROM usuario WHERE id_usuario=$id_encargado";
                if($resultado = $enlace->query($consulta)) {
                  while($row = $resultado->fetch_array()) {
                    
                    echo $row["nombre_usuario"];
                    echo " ".$row["apellido_usuario"]." ";
                  }
                  $resultado->close();
                }
        
              ?>
										</a>
			

               
                </form>
            </div>
        </nav>
	</div>
	
	   <div class="row">
			<div class="col-md-8">
			<div class="wrap" id="contenedor">
		<h1>Escoge un producto</h1>
		<div class="store-wrapper">
			<div class="category_list">
				<a href="#" class="category_item" category="all">Todo</a>
				<a href="#" class="category_item" category="verduras">Verduras</a>
				<a href="#" class="category_item" category="Frutas">Frutas</a>
				<a href="#" class="category_item" category="Especias">Hierbas y Especias</a>
				<a href="#" class="category_item" category="Secos">Frutos Secos</a>
				<a href="#" class="category_item" category="listos">Listos para Cocinar</a>
			</div>
			<section class="products-list">
			<?php
                          require("conexion.php");
                         
                          $consulta = "SELECT * FROM producto";
                          if($resultado = $enlace->query($consulta)) {
                            $c=1;
                            while($row = $resultado->fetch_array()) {?>
                            <div class="product-item" category="<?php echo $row['categoria_producto'];?>" >
					<img src="<?php echo$row['imagen_producto'];?>" alt="" >
					<p class="text-center">500 Grm.
					<?php echo$row['precio_producto']."Bs.";?><br>
					<?php echo $row["nombre_producto"];?></p>
					<form action="compra.php" method="post">
						<input type="text" value="<?php echo $row['id_producto'];?>" name="id_producto" hidden>
						<input type="text" value="<?php echo $id_encargado?>" name="id_usuario" hidden> 
						<input type="text" value="<?php echo $row['precio_producto'];?>" name="precio" hidden>
						<label for="cantidad">Cantidad</label>
						<input type="number" id="cantidad" name="cantidad" style="max-width:80px;" min="1">
						<button type="submit" >Añadir</button>
					</form>
					
				</div>
                          <?php
                             }
                             $resultado->close();
                           }
                          ?>
			
				
				
			</section>
		</div>
	</div>
			</div>
			<div class="col-md-4">
					<table class="table table-striped table-dark">
						
							<thead>
							  <tr>
								<th scope="col">#</th>
								<th scope="col">producto</th>
								<th scope="col">cantidad</th>
								<th></th>
								<th scope="col">precio unitario</th>
								
								<th scope="col" colspan="3">Operaciones</th>
							  </tr>
							</thead>
							<tbody>
							   
							<?php
							  require("conexion.php");
								$id_encargado;
							  $consulta = "SELECT d.nombre_producto,u.cantidad_compra,u.precio_compra,u.id_compra FROM compra u INNER JOIN producto d ON u.id_producto = d.id_producto WHERE u.id_usuario=$id_encargado";
							  if($resultado = $enlace->query($consulta)) {
								$c=1;
								while($row = $resultado->fetch_array()) {?>
								<tr>
								  <td><?php echo $c++?></td>
								  <td><?php echo $row["nombre_producto"];?></td>
								  <td>
									  <form action="actualizar.php" method="post">
									<input type="text" name="compra" value="<?php echo $row["id_compra"];?>" hidden>
									  <input type="number" value="<?php echo $row["cantidad_compra"];?>"  style="width:100px" name="cantidad">
									 
									

								</td>
								<td>
								<button type="submit" class="btn btn-warning" style="width:100px">Actulizar</button>
									  </form>
								</td>
								  <td><?php echo $row["precio_compra"];?></td>
								  
								 
								  <td><a href="eliminar.php?id=<?php echo $row["id_compra"];?>" class="btn btn-danger">Eliminar</a></td>
							  </tr>
							  <?php
								 }
								 $resultado->close();
							   }
							  ?>
							  
							  
							 </tbody>
							 
						  </table>
						  <button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Comprar tus Productos</button>
			</div>
			
		</div>
	
	<div id="id01" class="modal" style="max-width:500px;margin-left:40%;">
  
  <form class="modal-content animate" action="nuevo_producto.php" method="POST">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
      
    </div>

    <div class="container">
           
			<table border="1">
			<h1>Tus Compras</h1>
							<thead>
							  <tr>
								<th scope="col">#</th>
								<th scope="col">producto</th>
								<th scope="col">cantidad</th>
								<th scope="col">precio unitario</th>
								
								
							  </tr>
							</thead>
							<tbody>
							   
							<?php
							  require("conexion.php");
								$id_encargado;
							  $consulta = "SELECT d.nombre_producto,u.cantidad_compra,u.precio_compra,u.id_compra FROM compra u INNER JOIN producto d ON u.id_producto = d.id_producto WHERE u.id_usuario=$id_encargado";
							  if($resultado = $enlace->query($consulta)) {
								$c=1;
								$suma=0;
								while($row = $resultado->fetch_array()) {
								$suma+=$row["precio_compra"]*$row["cantidad_compra"];	
								?>
								<tr>
								  <td><?php echo $c++?></td>
								  <td><?php echo $row["nombre_producto"];?></td>
								  <td><?php echo $row["cantidad_compra"];?></td>
								  <td><?php echo $row["precio_compra"];?></td>
								  
								 
								 
							  </tr>
							  <?php
								 }
								 $resultado->close();
							   }
							  ?>
							  
							  <tr>
								 <td colspan="4">
								 Total de la compra es: <?php echo $suma;?>
								 </td> 
							  </tr>
							 </tbody>
							 
						  </table>
			
         
      
    </div>

    <div class="container" style="background-color:#f1f1f1">
	  <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancelar</button>
	  <button type="button" onclick="document.getElementById('id01').style.display='none'">Imprimir</button>
      
    </div>
  </form>
   
    <script type="text/javascript" src="js/jquery-1.4.2.js" ></script>
	<script type="text/javascript" src="js/cufon-yui.js"></script>
	<script type="text/javascript" src="js/cufon-replace.js"></script>
	<script type="text/javascript" src="js/Avenir_900.font.js"></script>
	<script type="text/javascript" src="js/Avenir_300.font.js"></script>
    <script type="text/javascript" src="js/Avenir_500.font.js"></script>
    <script src="js/javascrip.js"></script>
</body>
</html>
<?php
} else {
    header("Location: login.html");
}
?>